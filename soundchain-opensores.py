from FoxDot import *

import os
import time
import json
import random
import base64
import re
from atomicwrites import atomic_write

Scale.default.set("aeolian")

# initializing value dictionaries

valdict = {}
valdict["bpm"]=100
valdict["howoften"]=4
valdict["nopitchshift"] = 3
valdict["dur1"] = 4
valdict["dur2"] = 3
valdict["dur3"] = 2
valdict["drive"]=0
valdict["durall"]=10
valdict["cut"]=0
valdict["room"]=0
valdict["extranotes"]=[0,0]
valdict["sus"]=0.5
valdict["shape"]=0
valdict["amp_b1"] = 0 # BASS 0.4
valdict["amp_d1"] = 0 # D1 0.3
valdict["amp_d3"] = 0# 0.3
valdict["amp_s1"] = 0
valdict["amp_r1"] = 0  # 4
valdict["amp_r2"] = 0  # 3
valdict["amp_r3"] = 0  #2

# value change steps (how much they change when incrementing or decreasing)

stepdict = {}
stepdict["bpm"]=5
stepdict["howoften"]=1
stepdict["nopitchshift"] = 1
stepdict["dur1"] = 1
stepdict["dur2"] = 1
stepdict["dur3"] = 1
stepdict["durall"]=1
stepdict["drive"]=0.1
stepdict["cut"] = 0.1
stepdict["room"] = 0.5
stepdict["sus"] = 0.1
stepdict["shape"] = 0.1

# max values

maxdict = {}
maxdict["bpm"]=140
maxdict["howoften"]=10
maxdict["nopitchshift"]=10
maxdict["dur1"] = 8
maxdict["dur2"] = 8
maxdict["dur3"] = 8
maxdict["durall"] = 18
maxdict["drive"] = 0.4
maxdict["cut"]=1
maxdict["room"]=4
maxdict["sus"]=1
maxdict["shape"]=0.3
maxdict["amp_b1"] = 0.4 # BASS 0.4
maxdict["amp_d1"] = 0.2 # D1 0.3
maxdict["amp_d3"] = 0 # 0.3
maxdict["amp_s1"] = 3
maxdict["amp_r1"] = 2  # 4
maxdict["amp_r2"] = 1  # 3
maxdict["amp_r3"] = 2  #2

# mean values

mindict = {}
mindict["bpm"]=40
mindict["howoften"]=1
mindict["nopitchshift"]=1
mindict["dur1"] = 1
mindict["dur2"] = 1
mindict["dur3"] = 1
mindict["durall"] = 6
mindict["drive"] = 0
mindict["cut"]=0
mindict["room"]=0
mindict["sus"]=0.1
mindict["shape"]=0

# initializing musical variables (beat and melody)

beatstring = ""
tones = []

# lists of topics that trigger given acctions 
# improvement needs new topic model and new interpretation of topics

t_finvalue = [2,3,7,29,54]
t_currency = [12,49]
t_networks = [11, 14, 25]
t_bounty = [28, 30]
t_world = [28, 30]
t_tech = [5,27,39,40,41,47]
t_giants = [53]
t_society = [0]
t_application = [13, 35]
t_security = [23]
t_mining = [4]

# parameters to change - improvement needed here!

parameters = ["drive", "dur1", "nopitchshift", "dur2","dur3", "durall", "bpm",
"howoften", "cut", "room", "sus", "shape"]

# initializing temporary vars

changedict = {}
tmp = ""

numbers = None

playdrones = True


## what the hell is this?

# HPFilter on
Master().hpf=4000#var([0,4000],[76,4])

# HPFilter off
# REMEMBER THIS STARTS ON
Master().hpf=var([0,4000],[72,8])

# VOLUME CONTROL
ampdict = {}
ampdict["b1"] = 0.2# BASS 0.4
ampdict["d1"] = 0.1 # D1 0.2
ampdict["d3"] = 0 # 0.3
ampdict["s1"] = 0.1
ampdict["r1"] = 0.6  # 0.7
ampdict["r2"] = 0.5  # 0.5
ampdict["r3"] = 0.5  # 0.5

# mega huge "action" on text function that needs to be broken down

def action(text):
    global drive, nopitchshift, tones, ampdict, valdict, tmp, beatstring
    for i in parameters:
        changedict[i]=0
    topiclist = json.loads(text)
    if beatstring == "":
        beatstring = base64.b64encode(text.encode("utf-8"))
        beatstring = beatstring.decode("utf-8")
    if tones == []:
        tones = [ord(_) % 35 for _ in beatstring]
    changestr = "\n"
    # changes to the sounds according to the topics identified in the tweet (topiclist)
    # this will populate the temporary changedict variable 
    for t in topiclist[0]:
        if t in t_finvalue:
            changedict["drive"] +=1
            changedict["room"] -= 1
            changedict["sus"] += 1
            changedict["bpm"] -= 1
            changedict["durall"] -=1
        if t in t_networks or t in t_tech:
            changedict["drive"] -=2
            changedict["nopitchshift"] -= 1
            changedict["room"] += 1
            changedict["durall"] -=1
            valdict["extranotes"] = [0,0,0,2,0,0,0,5]
            changedict["sus"] -= 1
            changedict["shape"] -= 1
            changedict["bpm"] += 1
        if t in t_bounty:
            changedict["drive"] +=1
            changedict["cut"] +=1
            changedict["room"] -= 1
            changedict["dur1"] +=1
            changedict["dur2"] -=1
            changedict["shape"] += 1
            valdict["extranotes"] = [0,0,0,0,5]
        if t in t_currency:
            changedict["howoften"] +=1
            changedict["durall"] -=1
            changedict["dur3"] -=1
            changedict["room"] += 1
            changedict["dur1"] -=1
            changedict["dur2"] +=1
            valdict["extranotes"] = []
            changedict["sus"] += 1
            changedict["shape"] += 1
        if t in t_giants:
            changedict["drive"] +=1
            changedict["shape"] -= 1
            changedict["nopitchshift"] -= 1
            changedict["room"] -= 1
        if t in t_society:
            changedict["drive"] -= 1
            changedict["shape"] -= 1
            changedict["bpm"] -= 1
            changedict["durall"] +=1
        if t in t_security:
            changedict["drive"] -= 1
            changedict["shape"] -= 1
            changedict["bpm"] -= 1
    for p in parameters:
        sign = ""
        if changedict[p] > 0:
            valdict[p] += stepdict[p]
            numbers = str(valdict[p])
            if valdict[p] > maxdict[p]: 
                valdict[p] = maxdict[p]
            else:
                sign = "+"
        if changedict[p] < 0:
            valdict[p] -= stepdict[p]
            numbers = str(valdict[p])
            if valdict[p] < mindict[p]: 
                valdict[p] = mindict[p]
            else:
                sign = "-"
        changestr += p.upper() + " "*(15-len(p)) + str(round(valdict[p],2))
        changestr += " " * (5 - len(str(round(valdict[p],2)))) + sign + "\n\n"
    #if printvals: print(valdict)
    if changestr== "\n": changestr = "\nNONE"
    with atomic_write("/home/soundchain/soundchain/changepar_tmp", overwrite=True) as file:
        file.write("Soundchain parameters:\n".upper() + changestr + "\n" * 5)
        os.system("mv /home/soundchain/soundchain/changepar_tmp /home/soundchain/soundchain/changepar")

# mega huge function that reads and applies the changes, with foxdot instructions

def read():
    global drive, nopitchshift, beatstring, tones, ampdict, valdict, tmp, playdrones
    #print(int(Clock.now()))
    if int(Clock.now()) % 1000 == 0:
        Clock.clear()
        beatstring = ""
        tones = []
        time.sleep(30)
    #if any([int(Clock.now()) % _ == 0 for _ in range(450,500)]): 
    #    Clock.clear
    #    return(True)
#    global drive, nopitchshift, beatstring, tones, ampdict, valdict, tmp, playdrones
    if playdrones:
        r1 >> klank(b1.pitch, oct=1,
         sus=[2,4,2,4], dur=[2,4,2,4], blur = 1,
         drive=valdict["drive"]*0.55,
         shape=0,
         room=valdict["room"],
         amp=ampdict["r1"])
        r2 >> glass(b1.pitch, oct=1,
         sus=[2,4,2,2], dur=[2,4,2,2], blur = 1,
         drive=valdict["drive"]*0.55,
         shape=0,
         room=valdict["room"],
         amp=ampdict["r2"])
        r3 >> klank(b1.pitch, oct=2,
         sus=[2,2,4,2], dur=[2,2,4,2], blur = 1,
         drive=valdict["drive"]*0.55,
         shape=0, room=valdict["room"],
         amp=ampdict["r3"]*0.75)
        r4 >> glass(b1.pitch, oct=2,
         sus=[4,2,2,2], dur=[4,2,2,2], blur = 1,
         drive=valdict["drive"]*0.55,
         shape=0, room=valdict["room"],
         amp=ampdict["r3"]*0.75)
    playdrones = not playdrones
    # read the topics from a file and generate sounds from it
    with open("/home/soundchain/soundchain/topicnum", "rt") as in_file:
        text = in_file.read().strip()
        try:
            if tmp != text:
                # if the files have changed: 
                action(text)
                d1 >> play(beatstring,
                    rate=[1,1,0.8,1,0.4,1,1,1,1,0.7,1,1,1,1,1,1,1,0.3,1,1,1,1,1,1,1,1,1,1,1,0.2,1,1,1,1,1,1,1,1,1,1,0.1,1,1],
                    drive=valdict["drive"]*0.5,
                    sample=PRand(9),
                    dur=PDur([valdict["dur1"], valdict["dur2"], valdict["dur3"]], valdict["durall"]*8 ),
                    shape=valdict["shape"]*0.2,
                    echo=0,
                    room=valdict["room"],
                    amp=[ampdict["d1"]*v for v in [1,0.5,0,0.8,0,0,1,0,0]],
                    bpf=0).every(valdict["howoften"], "stutter", 4).every(
                    valdict["howoften"]*8, "reverse").often("stutter", 4).every(24, "palindrome").sometimes("amen").every(6, "rate.offmul", 3)
                b1 >> karp(var([0])+([0]*valdict["nopitchshift"]+tones) + valdict["extranotes"],
                     oct=(3,4,5),
                     dur=PDur([valdict["dur1"], valdict["dur2"], valdict["dur3"]], valdict["durall"] + 24),
                     rate=linvar([0,12],16),
                     room=valdict["room"]+0.5, echo = 0,
                     amp = ampdict["b1"]*PRand([0,1]),
                     cut=0, hpf = sinvar([0,4000], 16), hpr = P[1,1,0.].stretch(16),
                     drive = valdict["drive"]*0.5,).every(7, "offadd", 4).every(9, "shuffle")
                s1 >> pulse(b1.pitch,
                    dur=PDur([valdict["dur1"], valdict["dur2"], valdict["dur3"]], valdict["durall"]+64),
                    lpf = sinvar([100,-100], 24), hpr = P[1,1,0.].stretch(24),
                    shape=valdict["shape"]*0.1, oct=2, echo = valdict["cut"],
                    sus=valdict["sus"],
                    amp = ampdict["s1"]*PRand([0,1]),
                    drive=0.01 + valdict["drive"]*0.3,)
                d3 >> play("X o  Xo Xro  Xo X o fXo X o  Xod",
                                 sample=2,
                                 drive=valdict["drive"]*0.5,
                                 shape=valdict["shape"]*0.3,
                                 amp=ampdict["d3"]).often("stutter", 4).stop()
        except Exception as e:
            print("error", e, end= " ")
        tmp = text
        Clock.bpm = valdict["bpm"]
Clock.every(1, read)

t=input("press a key to exit")

